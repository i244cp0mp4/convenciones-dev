## Título
La primera oración deberá inicial con mayúsculas y tener una longitud no mayor a
50 caracteres sin punto final. Aquí deberá resumirse en una oración el cambio
que realiza el commit. Además deberá ir conjugado en forma imperativa; como guía
un buen título de commit siempre acompleta la frase: *Cuando este commit se
aplique...*
## Primer párrafo
Este párrafo empezará en la tercer línea y deberá responder la pregunta: Cuál/cuáles
fueron las causas o necesidades para que el commit existiera, por ejemplo:

`Se detectó que el método foo() no realizaba el correcto manejo de errores de
ejecución cuando la entrada era...`
## Segundo párrafo
En este párrafo irá una descripción contestando a la pregunta: *Cómo es que los
cambios resuelven el problema.*
## Tercer párrafo
Finalmente, el tercer párrafó servirá para explicar/detallar los *Efectos
secundarios de cambio* cuando este commit sea aplicado.
## Pie
Es importante, dar a conocer a forma de documentación, las fuentes que
consultamos cuando resolvimos el problema. Por esta razón, en el pie irán todas
aquellas referencias bibliográficas o electrónicas que dieron ideas al cambio.

No menos importante, si se discutieron y dieron puntos de vista dos o más
personas que resuelven el problema, están los Co-autores del cambio realizado.
Aquí se necesitará seguir este estilo:
```bash
Co-authored-by:
  Germán Camarena Flores <ger_flo@ciencias.unam.mx>;
  Diana Durán Valentino <vale_13@ciencias.unam.mx
```
### Fuentes
https://chris.beams.io/posts/git-commit/ (consultado el 22 Feb 2020)
